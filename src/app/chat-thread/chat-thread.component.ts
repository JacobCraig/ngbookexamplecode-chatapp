import { Component, OnInit, Input } from '@angular/core';
import { Thread } from '../thread/thread.model';
import { ThreadsService } from '../thread/threads.service';

@Component({
  selector: 'chat-thread',
  templateUrl: './chat-thread.component.html',
  styleUrls: ['./chat-thread.component.scss']
})
export class ChatThreadComponent implements OnInit {
  @Input() thread: Thread;

  public isCurrentThread: boolean = false;

  constructor(
    private ThreadsService: ThreadsService,
  ) {

  }

  ngOnInit() {
    this.ThreadsService.currentThread.subscribe(t => {
      this.isCurrentThread = t.id === this.thread.id;
    })
  }

  public clicked = (event) => {
    this.ThreadsService.setCurrentThread(this.thread);
  }
}
