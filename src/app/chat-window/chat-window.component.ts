import { Component, OnInit, ElementRef } from '@angular/core';
import { ThreadsService } from '../thread/threads.service';
import { combineLatest } from 'rxjs/operators';
import { MessagesService } from '../message/messages.service';
import { Observable } from 'rxjs';
import { Message } from '../message/message.model';
import { Thread } from '../thread/thread.model';
import { UsersService } from '../user/users.service';
import { User } from '../user/user.model';

@Component({
  selector: 'chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.scss']
})
export class ChatWindowComponent implements OnInit {
  public messages: Observable<Message[]>;
  public currentThread: Thread;
  public currentUser: User;

  public draft: string;

  constructor(
    private ThreadsService: ThreadsService,
    private MessagesService: MessagesService,
    private UsersService: UsersService,
    private ElementRef: ElementRef
  ) {
    this.messages = this.ThreadsService.currentThreadMessages;
    this.ThreadsService.currentThread.subscribe(t => this.currentThread = t);
    this.UsersService.currentUser.subscribe(u => this.currentUser = u);
  }

  ngOnInit() {
    this.messages.subscribe(() => {
      setTimeout(() => {
        var messageContainer: HTMLElement =  this.ElementRef.nativeElement.querySelector('.msg-container-base');
        messageContainer.scrollTop = messageContainer.scrollHeight;
      }, 0);
    });
  }

  public submitMessage = () => {
    var newMessage = new Message();
    newMessage.author = this.currentUser;
    newMessage.thread = this.currentThread;
    newMessage.text = this.draft;

    this.MessagesService.addMessage(newMessage);
    this.draft = '';
  }
}
