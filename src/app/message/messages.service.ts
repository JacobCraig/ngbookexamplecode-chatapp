import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { filter, scan, publishReplay, refCount, map } from 'rxjs/operators';
import { Message } from './message.model';
import { User } from '../user/user.model';
import { Thread } from '../thread/thread.model';
import * as _ from 'lodash';

const initialMessages: Message[] = [];

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  public newMessages: Subject<Message> = new Subject<Message>();
  public messages: Observable<Message[]>;

  private updates: Subject<any> = new Subject<any>();
  private create: Subject<Message> = new Subject<Message>();

  constructor() {
    this.messages = this.updates.pipe(
      scan((message: Message[], operation: IMessagesOperation) => {
        return operation(message);
      },
        initialMessages),
      publishReplay(1),
      refCount(),
    );

    this.create.pipe(
      map(this.getMessageAdder)
    ).subscribe(this.updates);

    this.newMessages.subscribe(this.create);
  }

  private getMessageAdder = (newMessage: Message): IMessagesOperation => {
    return (messages: Message[]): Message[] => {
      return messages.concat(newMessage);
    }
  }

  public addMessage = (message: Message): void => {
    this.newMessages.next(message);
  }

  public messagesForThreadUser = (thread: Thread, user: User) => {
    return this.newMessages.pipe(
      filter((message) => {
        return message.author.id !== user.id && message.thread.id === thread.id;
      }),
    );
  }
}

interface IMessagesOperation extends Function {
  (messages: Message[]): Message[];
}