import { Injectable } from '@angular/core';
import { User } from './user.model';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  public currentUser: Subject<User> = new BehaviorSubject<User>(null);

  public setCurrentUser = (newUser: User): void => {
    this.currentUser.next(newUser);
  }
}
