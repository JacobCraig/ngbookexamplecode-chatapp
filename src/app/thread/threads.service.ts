import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Thread } from './thread.model';
import { MessagesService } from '../message/messages.service';
import { map, combineLatest } from 'rxjs/operators';
import * as _ from 'lodash';
import { Message } from '../message/message.model';

@Injectable({
  providedIn: 'root'
})
export class ThreadsService {
  public threads: Observable<{ [key: string]: Thread }>;
  public currentThread: Subject<Thread> = new BehaviorSubject<Thread>(null);
  public orderedThreads: Observable<Thread[]>;
  public currentThreadMessages: Observable<Message[]>;


  constructor(
    private MessagesService: MessagesService,
  ) {
    this.threads = this.MessagesService.messages.pipe(
      map((messages) => {
        const nextThreads: { [key: string]: Thread } = {};

        messages.map((message) => {
          nextThreads[message.thread.id] = nextThreads[message.thread.id] || message.thread;

          const messagesThread: Thread = nextThreads[message.thread.id];
          if (
            messagesThread.lastMessage == null ||
            messagesThread.lastMessage.sentAt < message.sentAt) {
            messagesThread.lastMessage = message;
          }
        });

        return nextThreads;
      }),
    );

    this.orderedThreads = this.threads.pipe(
      map(threadGroups => {
        const threads: Thread[] = _.values(threadGroups);
        return _.sortBy(threads, (t: Thread) => t.lastMessage.sentAt).reverse();
      }),
    );

    this.currentThreadMessages = this.currentThread.pipe(
      combineLatest(this.MessagesService.messages, (thread, messages) => {
        if (thread != null && messages.length > 0) {
          return _.chain(messages)
            .filter((m: Message) => m.thread.id === thread.id)
            .map((m: Message) => {
              m.isRead = true;
              return m;
            })
            .value();
        }
        else {
          return [];
        }
      }),
    );
  }

  public setCurrentThread = (thread: Thread) => {
    this.currentThread.next(thread);
  }
}
