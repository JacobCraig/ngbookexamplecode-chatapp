import { Component, OnInit } from '@angular/core';
import { ThreadsService } from '../thread/threads.service';
import { Thread } from '../thread/thread.model';
import { Observable } from 'rxjs';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'chat-threads',
  templateUrl: './chat-threads.component.html',
  styleUrls: ['./chat-threads.component.scss']
})
export class ChatThreadsComponent implements OnInit {

  public threads: Observable<Thread[]>;

  constructor(
    public ThreadsService: ThreadsService,
  ) {
    this.threads = ThreadsService.orderedThreads;
  }

  ngOnInit() {
  }

}
