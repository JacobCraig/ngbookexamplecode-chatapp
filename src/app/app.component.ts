import { Component } from '@angular/core';
import { UsersService } from './user/users.service';
import { User } from './user/user.model';
import { MessagesService } from './message/messages.service';
import { Message } from './message/message.model';
import { ChatExampleData } from './data/chat-example-data';
import { Thread } from './thread/thread.model';
import { ThreadsService } from './thread/threads.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public currentUser: User;
  public newestMessage: Message;
  public currentThread: Thread;

  constructor(
    public UsersService: UsersService,
    public MessagesService: MessagesService,
    public ThreadsService: ThreadsService,
    private ChatExampleData: ChatExampleData,
    ) {
      this.UsersService.currentUser.subscribe(u => this.currentUser = u);
      this.MessagesService.newMessages.subscribe(m => this.newestMessage = m);
      this.ThreadsService.currentThread.subscribe(s => this.currentThread = s);
  }
}
