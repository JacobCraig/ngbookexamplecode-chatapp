import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from '../user/users.service';
import { User } from '../user/user.model';
import { Message } from '../message/message.model';

@Component({
  selector: 'chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.scss']
})
export class ChatMessageComponent implements OnInit {
  @Input() message: Message;
  public isUserMessage: boolean;

  constructor(
    private UsersService: UsersService,
  ) {
  }

  ngOnInit() {
    this.UsersService.currentUser.subscribe((u) => {
      this.isUserMessage = u != null && this.message != null && u.id === this.message.author.id;
    });
  }

}
