import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../message/messages.service';
import { ThreadsService } from '../thread/threads.service';
import { Thread } from '../thread/thread.model';
import { combineLatest } from 'rxjs/operators';
import * as _ from 'lodash';
import { Message } from '../message/message.model';

@Component({
  selector: 'chat-nav-bar',
  templateUrl: './chat-nav-bar.component.html',
  styleUrls: ['./chat-nav-bar.component.scss']
})
export class ChatNavBarComponent implements OnInit {
  public count: number = 0;

  constructor(
    MessagesService: MessagesService,
    ThreadsService: ThreadsService,
  ) {
    MessagesService.messages.pipe(
      combineLatest(ThreadsService.currentThread, (messages, currentThread) => [messages, currentThread]),
    ).subscribe(([messages, currentThread]: [Message[], Thread]) => {
      if (currentThread != null && messages.length > 0) {
        this.count = _.chain(messages)
        .filter((message: Message) => message.thread.id !== currentThread.id)
        .filter((message: Message) => !message.isRead)
        .value().length;
      }
      else {
        this.count = 0;
      }
    });
  }

  ngOnInit() {
  }

}
